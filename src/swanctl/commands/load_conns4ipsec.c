/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Copyright (C) 2014 Martin Willi
 * Copyright (C) 2014 revosec AG
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <limits.h>

#include "command.h"
#include "swanctl.h"
#include "load_conns.h"
#include "ipsec_api.h"


char* g_cur_ip = NULL;
char* g_remote_ip=NULL;
/**
 * Check if we should handle a key as a list of comma separated values
 */
static bool is_list_key(char *key)
{
	char *keys[] = {
		"local_addrs",
		"remote_addrs",
		"proposals",
		"esp_proposals",
		"ah_proposals",
		"local_ts",
		"remote_ts",
		"vips",
		"pools",
		"groups",
	};
	int i;

	for (i = 0; i < countof(keys); i++)
	{
		if (strcaseeq(keys[i], key))
		{
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * Check if we should handle a key as a list of comma separated files
 */
static bool is_file_list_key(char *key)
{
	char *keys[] = {
		"certs",
		"cacerts",
	};
	int i;

	for (i = 0; i < countof(keys); i++)
	{
		if (strcaseeq(keys[i], key))
		{
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * Add a vici list from a comma separated string value
 */
static void add_list_key(vici_req_t *req, char *key, char *value)
{
	enumerator_t *enumerator;
	char *token;

	vici_begin_list(req, key);
	enumerator = enumerator_create_token(value, ",", " ");
	while (enumerator->enumerate(enumerator, &token))
	{
		vici_add_list_itemf(req, "%s", token);
	}
	enumerator->destroy(enumerator);
	vici_end_list(req);
}

/**
 * Add a vici list of blobs from a comma separated file list
 */
static bool add_file_list_key(vici_req_t *req, char *key, char *value)
{
	enumerator_t *enumerator;
	chunk_t *map;
	char *token, buf[PATH_MAX];
	bool ret = TRUE;

	vici_begin_list(req, key);
	enumerator = enumerator_create_token(value, ",", " ");
	while (enumerator->enumerate(enumerator, &token))
	{
		if (!path_absolute(token))
		{
			if (streq(key, "certs"))
			{
				snprintf(buf, sizeof(buf), "%s%s%s",
						 SWANCTL_X509DIR, DIRECTORY_SEPARATOR, token);
				token = buf;
			}
			if (streq(key, "cacerts"))
			{
				snprintf(buf, sizeof(buf), "%s%s%s",
						 SWANCTL_X509CADIR, DIRECTORY_SEPARATOR, token);
				token = buf;
			}
		}

		map = chunk_map(token, FALSE);
		if (map)
		{
			vici_add_list_item(req, map->ptr, map->len);
			chunk_unmap(map);
		}
		else
		{
			fprintf(stderr, "loading %s certificate '%s' failed: %s\n",
					key, token, strerror(errno));
			ret = FALSE;
			break;
		}
	}
	enumerator->destroy(enumerator);
	vici_end_list(req);

	return ret;
}

char markbuf[32];
bool find_mark = false;
char childbuf[128];
bool find_child = false;
char ikebuf[128];

/**
 * Translate setting key/values from a section into vici key-values/lists
 */
static bool add_key_values(vici_req_t *req, settings_t *cfg, char *section)
{
	enumerator_t *enumerator;
	char *key, *value;
	bool ret = TRUE;

	enumerator = cfg->create_key_value_enumerator(cfg, section);
	while (enumerator->enumerate(enumerator, &key, &value))
	{
		if (is_list_key(key))
		{
			add_list_key(req, key, value);
			fprintf(stderr, "add_list_key key %s value %s\n", key, value);
		}
		else if (is_file_list_key(key))
		{
			ret = add_file_list_key(req, key, value);
			fprintf(stderr, "add_file_list_key key %s value %s\n", key, value);
		}
		else
		{
			if ((!find_mark) && (0 == strncmp("mark_in", key, sizeof("mark_in")))) {
				strncpy(markbuf, value, strlen(value));
				find_mark= true;
			}
			vici_add_key_valuef(req, key, "%s", value);
			fprintf(stderr, "xc add_key_valuef key %s value %s\n", key, value);
		}


		if (!ret)
		{
			break;
		}
	}
	enumerator->destroy(enumerator);

	return ret;
}


/**
 * Translate a settings section to a vici section
 */
static bool add_sections(vici_req_t *req, settings_t *cfg, char *section)
{
	enumerator_t *enumerator;
	char *name, buf[256];
	bool ret = TRUE;

	enumerator = cfg->create_section_enumerator(cfg, section);
	while (enumerator->enumerate(enumerator, &name))
	{
		vici_begin_section(req, name);
		snprintf(buf, sizeof(buf), "%s.%s", section, name);
		fprintf(stderr, "add section %s %s\n", section, name);
		if ((!find_child) && strstr(section, "children")) {
			strncpy(childbuf, name, strlen(name));
			fprintf(stderr, "find child %s\n", childbuf);
			find_child = true;
		}
		ret = add_key_values(req, cfg, buf);
		if (!ret)
		{
			break;
		}
		ret = add_sections(req, cfg, buf);
		if (!ret)
		{
			break;
		}
		vici_end_section(req);
	}
	enumerator->destroy(enumerator);

	return ret;
}

/**
 * Load an IKE_SA config with CHILD_SA configs from a section
 */
static bool load_conn(vici_conn_t *conn, settings_t *cfg,
					  char *section, command_format_options_t format)
{
	vici_req_t *req;
	vici_res_t *res;
	bool ret = TRUE;
	char buf[128];

	snprintf(buf, sizeof(buf), "%s.%s", "connections", section);

	strncpy(ikebuf, section, strlen(section));

	req = vici_begin("load-conn");

	vici_begin_section(req, section);

	// added by ipsec module
	// add local ipaddr
	vici_begin_list(req,"local_addrs");    
	vici_add_list_itemf(req,"%s", g_cur_ip); 
	vici_end_list(req);
	
	vici_begin_list(req,"remote_addrs");
	vici_add_list_itemf(req,"%s", g_remote_ip);
	vici_end_list(req);

	if (!add_key_values(req, cfg, buf) ||
		!add_sections(req, cfg, buf))
	{
		vici_free_req(req);
		return FALSE;
	}

	vici_end_section(req);

	res = vici_submit(req, conn);
	if (!res)
	{
		fprintf(stderr, "load-conn request failed: %s\n", strerror(errno));
		return FALSE;
	}
	if (format & COMMAND_FORMAT_RAW)
	{
		vici_dump(res, "load-conn reply", format & COMMAND_FORMAT_PRETTY,
				  stdout);
	}
	else if (!streq(vici_find_str(res, "no", "success"), "yes"))
	{
		fprintf(stderr, "loading connection '%s' failed: %s\n",
				section, vici_find_str(res, "", "errmsg"));
		ret = FALSE;
	}
	else
	{
		printf("loaded connection '%s'\n", section);
	}
	vici_free_res(res);
	return ret;
}

CALLBACK(list_conn, int,
	linked_list_t *list, vici_res_t *res, char *name, void *value, int len)
{
	if (streq(name, "conns"))
	{
		char *str;

		if (asprintf(&str, "%.*s", len, value) != -1)
		{
			list->insert_last(list, str);
		}
	}
	return 0;
}

/**
 * Create a list of currently loaded connections
 */
static linked_list_t* list_conns(vici_conn_t *conn,
								 command_format_options_t format)
{
	linked_list_t *list;
	vici_res_t *res;

	list = linked_list_create();

	res = vici_submit(vici_begin("get-conns"), conn);
	if (res)
	{
		if (format & COMMAND_FORMAT_RAW)
		{
			vici_dump(res, "get-conns reply", format & COMMAND_FORMAT_PRETTY,
					  stdout);
		}
		vici_parse_cb(res, NULL, NULL, list_conn, list);
		vici_free_res(res);
	}
	return list;
}

/**
 * Remove and free a string from a list
 */
static void remove_from_list(linked_list_t *list, char *str)
{
	enumerator_t *enumerator;
	char *current;

	enumerator = list->create_enumerator(list);
	while (enumerator->enumerate(enumerator, &current))
	{
		if (streq(current, str))
		{
			list->remove_at(list, enumerator);
			free(current);
		}
	}
	enumerator->destroy(enumerator);
}

/**
 * Unload a connection by name
 */
static bool unload_conn(vici_conn_t *conn, char *name,
					    command_format_options_t format)
{
	vici_req_t *req;
	vici_res_t *res;
	bool ret = TRUE;

	req = vici_begin("unload-conn");
	vici_add_key_valuef(req, "name", "%s", name);
	res = vici_submit(req, conn);
	if (!res)
	{
		fprintf(stderr, "unload-conn request failed: %s\n", strerror(errno));
		return FALSE;
	}
	if (format & COMMAND_FORMAT_RAW)
	{
		vici_dump(res, "unload-conn reply", format & COMMAND_FORMAT_PRETTY,
				  stdout);
	}
	else if (!streq(vici_find_str(res, "no", "success"), "yes"))
	{
		fprintf(stderr, "unloading connection '%s' failed: %s\n",
				name, vici_find_str(res, "", "errmsg"));
		ret = FALSE;
	}
	vici_free_res(res);
	return ret;
}

/**
 * See header.
 */
int load_conns_cfg(vici_conn_t *conn, command_format_options_t format,
				   settings_t *cfg)
{
	u_int found = 0, loaded = 0, unloaded = 0;
	char *section;
	enumerator_t *enumerator;
	linked_list_t *conns;

	conns = list_conns(conn, format);

	enumerator = cfg->create_section_enumerator(cfg, "connections");
	while (enumerator->enumerate(enumerator, &section))
	{
		remove_from_list(conns, section);
		found++;
		if (load_conn(conn, cfg, section, format))
		{
			loaded++;
		}
	}
	enumerator->destroy(enumerator);

	/* unload all connection in daemon, but not in file */
	while (conns->remove_first(conns, (void**)&section) == SUCCESS)
	{
		if (unload_conn(conn, section, format))
		{
			unloaded++;
		}
		free(section);
	}
	conns->destroy(conns);

	if (format & COMMAND_FORMAT_RAW)
	{
		return 0;
	}
	if (found == 0)
	{
		printf("no connections found, %u unloaded\n", unloaded);
		return 0;
	}
	if (loaded == found)
	{
		printf("successfully loaded %u connections, %u unloaded\n",
			   loaded, unloaded);
		return 0;
	}
	fprintf(stderr, "loaded %u of %u connections, %u failed to load, "
			"%u unloaded\n", loaded, found, found - loaded, unloaded);
	return EINVAL;
}

int load_conns(vici_conn_t *conn, char* conf_suffix, char* cur_ip, char* remote_ip,char** ike,
		char** child, char** mark)
{
	command_format_options_t format = COMMAND_FORMAT_PRETTY; //COMMAND_FORMAT_RAW
	settings_t *cfg;
	int ret;
	g_cur_ip = cur_ip;
	g_remote_ip = remote_ip;
	// clean both ikebuf and childbuf everytime entry loading conf
	memset(childbuf, 0, sizeof(childbuf)/sizeof(char));
	memset(ikebuf, 0, sizeof(ikebuf)/sizeof(char));

	char conf_file[256];
	if (strlen(SWANCTL_CONF) >= CONF_FILE_MAXLEN) {
		fprintf(stderr, "conf file too long\n");
		return EINVAL;
	}
	if (conf_suffix == NULL) {
		fprintf(stderr, "wrong conf suffix\n");
		return EINVAL;
	}
	if (strlen(conf_suffix) >= CONF_SUFFIX_MAXLEN) {
		fprintf(stderr, "conf suffix too long\n");
		return EINVAL;
	}
	strcpy(conf_file, SWANCTL_CONF);
	strcat(conf_file, conf_suffix);

	fprintf(stderr, "parsing '%s'\n", conf_file);
	cfg = settings_create(conf_file);
	if (!cfg)
	{
		fprintf(stderr, "parsing '%s' failed\n", conf_file);
		return EINVAL;
	}

	ret = load_conns_cfg(conn, format, cfg);

	cfg->destroy(cfg);

	// passing info parsed from conf file back to ipsec plugin
	if (find_child) {
		fprintf(stderr, "assign buf to child %s\n", childbuf);
		*child = childbuf;
		find_child = false;
	}
	if (find_mark) {
		fprintf(stderr, "assign buf to mark %s\n", markbuf);
		*mark = markbuf;
		find_mark = false;
	}
	*ike = ikebuf;
	fprintf(stderr, "assign buf to conn %s\n", ikebuf);

	return ret;
}
