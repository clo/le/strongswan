/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "command.h"

#include <errno.h>

#define PART_CHILD 1
#define PART_IKE 2

int part_terminate(vici_conn_t *conn, int part, const char* name)
{
	vici_req_t *req;
	vici_res_t *res;
	const char *arg, *child = NULL, *ike = NULL;
	int ret = 0, level = 1;

	if (part == PART_CHILD)
		child = name;
	if (part == PART_IKE)
		ike = name;

	req = vici_begin("terminate");
	if (child)
	{
		vici_add_key_valuef(req, "child", "%s", child);
	}
	if (ike)
	{
		vici_add_key_valuef(req, "ike", "%s", ike);
		fprintf(stderr, "terminate ike %s\n", ike);
	}
	vici_add_key_valuef(req, "loglevel", "%d", level);

	res = vici_submit(req, conn);
	if (!res)
	{
		ret = errno;
		fprintf(stderr, "terminate request failed: %s\n", strerror(errno));
		return ret;
	}
	
	if (streq(vici_find_str(res, "no", "success"), "yes"))
	{
		printf("terminate completed successfully\n");
		fprintf(stderr, "terminate completed successfully\n");
	}
	else
	{
		fprintf(stderr, "terminate failed: %s\n",
				vici_find_str(res, "", "errmsg"));
		ret = 1;
	}
	vici_free_res(res);
	return ret;
}

int terminate(vici_conn_t *conn, const char* name)
{
        return part_terminate(conn, PART_IKE, name);
}
